knot-resolver (3.2.0-1) unstable; urgency=medium

  * new upstream release
  * bump ABI from libkres8 to libkres9
  * drop reproducibility patch, adopted upstream
  * include experimental_dot_auth module (recommend: lua-basexx for it)
  * d/copyright: removed reference to ISAAC, removed upstream

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 19 Dec 2018 19:23:37 -0500

knot-resolver (3.1.0-1) unstable; urgency=medium

  * new upstream release
  * set knot-resolver-doc to Multi-Arch: foreign
  * drop upstreamed patch; avoid use of git during installcheck
  * make reproducible datestamp in kresd(8)
  * increase build-dep on libknot to 2.7.2
  * updated libkres8.symbols
  * minimize upstream signing keys
  * drop unused lintian-overrides

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 20 Nov 2018 15:47:19 -0500

knot-resolver (3.0.0-9) unstable; urgency=medium

  * autopkg: allow stderr on "make installcheck"

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 12 Sep 2018 13:02:45 -0400

knot-resolver (3.0.0-8) unstable; urgency=medium

  * avoid stderr during autopkgtest
  * refresh patches

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 12 Sep 2018 09:51:40 -0400

knot-resolver (3.0.0-7) unstable; urgency=medium

  * try upstream proposed test suite change

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 11 Sep 2018 23:58:14 -0400

knot-resolver (3.0.0-6) unstable; urgency=medium

  * stop building knot-resolver on arm64 (Closes: #907729)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 05 Sep 2018 19:30:10 -0400

knot-resolver (3.0.0-5) unstable; urgency=medium

  * roundtrip test: increase verbosity
  * roundtrip test: avoid long timeouts if UDP query fails
  * autopkgtest: improve call to "make installcheck"

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 31 Aug 2018 21:10:39 -0400

knot-resolver (3.0.0-4) unstable; urgency=medium

  * Re-enable tests on all architectures
  * autopkgtest: be more verbose about error
  * autopkgtest: allow specifying module directory
  * point to modules needed to run roundtrip test at build time

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 31 Aug 2018 14:00:24 -0400

knot-resolver (3.0.0-3) unstable; urgency=medium

  * improve autopkgtest
  * autopkgtest: env vars can select which kdig and kresd to test
  * run roundtrip test at compile time too

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 31 Aug 2018 11:36:17 -0400

knot-resolver (3.0.0-2) unstable; urgency=medium

  * autopkgtest: added full roundtrip tests

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 30 Aug 2018 14:41:02 -0400

knot-resolver (3.0.0-1) unstable; urgency=medium

  * new upstream release
  * Standards-Version: bump to 4.2.1 (no changes needed)
  * build-depend on libknot 2.7 or later

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 27 Aug 2018 18:42:45 -0400

knot-resolver (2.4.1-1) unstable; urgency=medium

  * new upstream release
   - resolves CVE-2018-10920
  * k-r.postinst: strip trailing whitespace
  * clean up patches already adopted upstream
  * ship rebinding lua module
  * use upstream-preferred systemd service configuration
  * tell lintian not to complain about kresd.target
  * tighten up build-deps on libknot-dev
  * three symbols (re)introduced into libkres7
  * Standards-Version: bump to 4.2.0 (no changes needed)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 27 Aug 2018 18:22:48 -0400

knot-resolver (2.3.0-4) unstable; urgency=medium

  * try to restart any running kresd daemon on upgrade
  * correct systemctl start documentation (Closes: #891838)
  * fix FTBFS on kfreebsd (Closes: #900013)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 24 May 2018 13:10:31 -0400

knot-resolver (2.3.0-3) unstable; urgency=medium

  * create knot-resolver user correctly in postinst (Closes: #894580)
  * d/changelog: update to acknowledge CVE-2018-1110

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 14 May 2018 11:58:29 -0400

knot-resolver (2.3.0-2) unstable; urgency=medium

  * only build against libsystemd on linux architectures

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 26 Apr 2018 11:56:29 -0400

knot-resolver (2.3.0-1) unstable; urgency=medium

  * new upstream release
    - resolves CVE-2018-1110
  * drop patches already upstream
  * ship prefill module
  * d/copyright: adjust years of main stanza
  * add trie_* and kr_zonecut_is_empty to symbols
  * Standards-Version: bump to 4.1.4 (no changes needed)
  * drop unnecessary build-dependencies

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 24 Apr 2018 19:20:45 -0400

knot-resolver (2.2.0-1) unstable; urgency=medium

  * new upstream release
    - move from libkres6 to libkres7 due to ABI change
  * drop patches already merged upstream
  * avoid shipping root.hints and icann-ca.pem without a patch
  * work around https://gitlab.labs.nic.cz/knot/knot-resolver/issues/338
  * renumbering patches for cleanliness
  * use externally-set CPPFLAGS during build
  * clean up build of lua-aho-corasick
  * document copyright better to drop a lintian override
  * clarify rationales behind lintian-overrides
  * be clearer about keyfile-ro restarting kresd in kresd.8

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 08 Apr 2018 23:46:47 -0400

knot-resolver (2.1.1-1) unstable; urgency=medium

  * new upstream release
  * drop patches already upstream
  * refresh remaining patches

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 25 Feb 2018 17:36:46 -0800

knot-resolver (2.1.0-2) unstable; urgency=medium

  * clean up README.source
  * more bugfix patches from upstream

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 22 Feb 2018 13:27:59 -0800

knot-resolver (2.1.0-1) unstable; urgency=medium

  * new upstream release
    - move from libkres5 to libkres6 due to an ABI change
    - kresd.systemd(7) documents running a public-facing resolver
      (Closes: #880682)
  * d/upstream/signing-key.asc: add Tomas Krizek's key
  * d/gbp.conf: use DEP-14 branch naming
  * drop patches already upstream
  * d/control: build-depend on libknot-dev (>= 2.6.4)
  * handle updates to the DNS root.key sensibly (dpkg triggers)
  * systemd: mask kresd.service, following upstream's recommendation
  * /etc/default/kresd: indicate that it is only for sysvinit
  * d/rules: clean up unnecessary lines
  * systemd: ship upstream unit files and tmpfiles config directly
  * d/control: Rules-Requires-Root: no
  * use python3 during the build.
  * ship upstream NEWS file as upstream changelog
  * debian/TODO: clean up

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 17 Feb 2018 21:13:45 -0500

knot-resolver (2.0.0-3) unstable; urgency=medium

  * documentation: build with sphinx_rtd_theme

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 05 Feb 2018 17:13:04 -0500

knot-resolver (2.0.0-2) unstable; urgency=medium

  * Simplify /etc/default/kresd
  * drop preinst special case for versions < 1.1.0-2
  * postinst: create the knot-resolver user at configure time
  * initscript knows path to /usr/lib/tmpfiles.d/knot-resolver.conf
  * Overhaul systemd integration.
  * d/copyright: include more contributed subprojects (Closes: #889646)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 05 Feb 2018 13:46:25 -0500

knot-resolver (2.0.0-1) unstable; urgency=medium

  [ Ondřej Surý ]
  * Reduce the (non-public) symbols in libkres4

  [ Daniel Kahn Gillmor ]
  * import systemd fixes from Tomas Krizek
  * avoid leaving /run/knot-resolver world-executable
  * systemd: set up kresd@1 service and sockets
  * wrap-and-sort -ast
  * Add kresd.service as a symlink to kresd@1.service
  * more bugfixes from upstream
  * NEWS: document changes for using kresd with systemd
  * include new lua modules
  * move from libkres4 to libkres5 (cache functions dropped)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 05 Feb 2018 00:25:51 -0500

knot-resolver (1.5.3-1) unstable; urgency=medium

  * New upstream release

 -- Ondřej Surý <ondrej@debian.org>  Wed, 24 Jan 2018 12:35:55 +0000

knot-resolver (1.5.2-1) unstable; urgency=medium

  * New upstream release
    - closes CVE-2018-1000002
  * d/copyright: use https where possible
  * move to debhelper 11 (docs to /u/s/doc/knot-resolver/, policy §12.3)
  * d/control: move Maintainer: to knot-resolver@packages.debian.org

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 22 Jan 2018 15:00:21 +0100

knot-resolver (1.5.1-2) unstable; urgency=medium

  * libkres-dev Depends on libkres4 (Closes: #887572)
  * Standards-Version: bump to 4.1.3 (no changes needed)
  * move Vcs-* to salsa.debian.org

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 17 Jan 2018 23:23:45 -0500

knot-resolver (1.5.1-1) unstable; urgency=medium

  * new upstream release
  * move packaging split to unstable
  * d/watch: move to version 4, avoid alpha/beta releases
  * drop patches for bugs already fixed upstream
  * use "make installcheck" as a simple test
  * track evolving name of etcd module
  * wrap-and-sort -a
  * shipping new modules: priming, detect_time_{jump,skew}
  * added kr_now to exported symbols
  * Standards-Version: bump to 4.1.2 (no changes needed)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 13 Dec 2017 15:40:10 -0500

knot-resolver (1.5.0-5) experimental; urgency=medium

  * packaging cleanup, splitting out library packages
  * dbgsym migration no longer needed
    knot-resolver-dbg is not shipped anywhere
  * clean up installation of knot-resolver-module-http
  * move to dh_missing, as dh_install --fail-missing is deprecated
  * clean up/simplify knot-resolver.install
  * break out libkres library packages
  * put manpage installation under control of dh_installman
  * knock stuff off the packaging todo list
  * register knot-resolver-doc with doc-base
  * add libkres4.symbols to track API evolution
  * correct debian/copyright paths

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 30 Nov 2017 23:25:54 -0500

knot-resolver (1.5.0-4) unstable; urgency=medium

  * Pull upstream patch to increase the CACHE_SIZE in tests_cache for
    platforms with page size greater than 4k (Closes: #878976)
  * Pull upstream patch from MR!389 to not run check-config at all as
    modulepath is hardcoded (Closes: #881492)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 13 Nov 2017 19:53:30 +0000

knot-resolver (1.5.0-3) unstable; urgency=medium

  * reorder tests

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 13 Nov 2017 03:24:06 +0800

knot-resolver (1.5.0-2) unstable; urgency=medium

  * include a list of suggestions for future packaging work
  * try some debugging to see failures for #881492

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 13 Nov 2017 03:19:21 +0800

knot-resolver (1.5.0-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: update source homepage (Closes: #879166)
  * Standards-Version: bump to 4.1.1 (no changes needed)
  * switch to debhelper 10
  * drop dh-systemd build-dep -- no longer needed (may be added back for
    backports)
  * [lintian] fix debian-news-entry-uses-asterisk
  * drop unused lintian overrides
  * added myself to uploaders
  * update upstream signer key
  * patch from upstream for SIGPIPE (Closes: #881462)
  * patch to fix upstream test suite
  * debian/copyright: cleanup
  * move to python3-{sphinx,breathe} for build-dep
  * use root.hints from dns-root-data

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 12 Nov 2017 13:00:50 +0800

knot-resolver (1.4.0-2) unstable; urgency=medium

  * Add root.hints into the package
  * Bump required libknot version to >= 2.6.0

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Sep 2017 19:56:14 +0200

knot-resolver (1.4.0-1) unstable; urgency=medium

  * Make libsystemd-dev (>= 227) a hard requirement
  * Fix the Knot Resolver homepage
  * New upstream version 1.4.0
  * aho-corrasick.lua has been removed from the sources

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Sep 2017 18:31:54 +0200

knot-resolver (1.3.3-1) unstable; urgency=medium

  * New upstream version 1.3.3

 -- Ondřej Surý <ondrej@debian.org>  Wed, 09 Aug 2017 15:28:35 +0200

knot-resolver (1.3.2-1) unstable; urgency=medium

  * New upstream version 1.3.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 28 Jul 2017 14:22:08 +0200

knot-resolver (1.3.1-1) unstable; urgency=medium

  * New upstream version 1.3.1
  * Remove all upstream patches merged upstream

 -- Ondřej Surý <ondrej@debian.org>  Fri, 23 Jun 2017 14:38:15 +0200

knot-resolver (1.3.0-2) unstable; urgency=medium

  * Fix the moduledir in the http2 module

 -- Ondřej Surý <ondrej@debian.org>  Fri, 16 Jun 2017 13:20:14 +0200

knot-resolver (1.3.0-1) unstable; urgency=medium

  [ Ondřej Surý ]
  * New upstream version 1.3.0
  * Export MAKEVARS as environment variables
  * Add libedit-dev to B-D to build kresc

  [ Daniel Kahn Gillmor ]
  * ship kresc because we use --fail-missing
  * clean up things the upstream build scripts leave behind

 -- Ondřej Surý <ondrej@debian.org>  Tue, 13 Jun 2017 09:07:54 +0200

knot-resolver (1.2.6-1) unstable; urgency=medium

  * New upstream version 1.2.6

 -- Ondřej Surý <ondrej@debian.org>  Mon, 24 Apr 2017 16:31:11 +0200

knot-resolver (1.2.5-1) unstable; urgency=medium

  * New upstream version 1.2.5

 -- Ondřej Surý <ondrej@debian.org>  Wed, 05 Apr 2017 16:07:40 +0200

knot-resolver (1.2.4-1) unstable; urgency=medium

  * New upstream version 1.2.4

 -- Ondřej Surý <ondrej@debian.org>  Thu, 09 Mar 2017 14:20:24 +0100

knot-resolver (1.2.3-1) unstable; urgency=medium

  * New upstream version 1.2.3

 -- Ondřej Surý <ondrej@debian.org>  Thu, 23 Feb 2017 15:49:14 +0100

knot-resolver (1.2.2-1) unstable; urgency=medium

  * New upstream version 1.2.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 10 Feb 2017 13:54:28 +0100

knot-resolver (1.2.1-1) unstable; urgency=high

  * New upstream version 1.2.1

 -- Ondřej Surý <ondrej@debian.org>  Wed, 01 Feb 2017 20:56:35 +0100

knot-resolver (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0
  * Squash 1.2.0 changelog into a single 1.2.0 entry
  * Add d/upstream/signing-key.asc
  * Add d/watch to monitor upstream tarballs

 -- Ondřej Surý <ondrej@debian.org>  Wed, 25 Jan 2017 14:23:27 +0100

knot-resolver (1.1.1-131-g59d5adf-1) unstable; urgency=medium

  * Disable tests on mipsel that started to fail in a mysterious way
    (Closes: #847696)
  * Imported Upstream version 1.1.1-131-g59d5adf

 -- Ondřej Surý <ondrej@debian.org>  Wed, 14 Dec 2016 12:16:36 +0100

knot-resolver (1.1.1-122-g940fda9-2) unstable; urgency=medium

  * Move -DNDEBUG just to build target

 -- Ondřej Surý <ondrej@debian.org>  Tue, 13 Dec 2016 09:52:19 +0100

knot-resolver (1.1.1-122-g940fda9-1) unstable; urgency=medium

  * Imported Upstream version 1.1.1-122-g940fda9
  * Remove static linking patch
  * Add $(MAKEVARS) to make check invocation
  * Use SOVERSION when compiling to unfail the tests

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Dec 2016 16:26:27 +0100

knot-resolver (1.1.1-41-g8437a7d-2) unstable; urgency=medium

  * Update NEWS with kresd.service (Closes: #843178)
  * Enable NDEBUG on production builds

 -- Ondřej Surý <ondrej@debian.org>  Wed, 07 Dec 2016 10:02:55 +0100

knot-resolver (1.1.1-41-g8437a7d-1) unstable; urgency=medium

  * Imported Upstream version 1.1.1-41-g8437a7d
  * Add instructions how to rebuild epoch.js and epoch.css using Debian
    tools (Closes: #833309)
  * Record SONAMEs of used C libraries as lua literals instead of guessing
    (Closes: #841496)
  * Rebase patches on top of new git master release

 -- Ondřej Surý <ondrej@debian.org>  Fri, 21 Oct 2016 11:27:56 +0200

knot-resolver (1.1.1-2) unstable; urgency=medium

  * Add missing sources for epoch.js
  * Also allow loading of libknot.so.4

 -- Ondřej Surý <ondrej@debian.org>  Tue, 11 Oct 2016 14:54:20 +0200

knot-resolver (1.1.1-1) unstable; urgency=medium

  * Imported Upstream version 1.1.1

 -- Ondřej Surý <ondrej@debian.org>  Tue, 11 Oct 2016 14:52:37 +0200

knot-resolver (1.1.0-8) unstable; urgency=medium

  * Use restart instead of try-restart if upgrading from << 1.1.0-7

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Aug 2016 11:18:46 +0200

knot-resolver (1.1.0-7) unstable; urgency=medium

  * Add the final missing bits from dh_systemd_enable

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Aug 2016 10:29:22 +0200

knot-resolver (1.1.0-6) unstable; urgency=medium

  * Make extra sure that kresd.service is stopped before starting the
    sockets again
  * Stop old systemd units early before they are removed

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Aug 2016 08:32:22 +0200

knot-resolver (1.1.0-5) unstable; urgency=medium

  * Stop kresd.service in prerm script to allow kresd.socket restarts

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Aug 2016 15:43:59 +0200

knot-resolver (1.1.0-4) unstable; urgency=medium

  * Install kresd-tls.socket into the package
  * Unwrap variables in default file as systemd units cannot resolve
    nested variables

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Aug 2016 13:29:32 +0200

knot-resolver (1.1.0-3) unstable; urgency=medium

  * Tighten Build-Depends on libcmocka-dev to 1.0.0
  * Cleanup the knot-resolver -> kresd init script rename

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Aug 2016 13:11:10 +0200

knot-resolver (1.1.0-2) unstable; urgency=medium

  * Update systemd units to be named after the daemon (kresd)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 15 Aug 2016 10:09:53 +0200

knot-resolver (1.1.0-1) unstable; urgency=medium

  * Imported Upstream version 1.1.0
  * Rebase patches on top of Knot Resolver 1.1.0
  * Don't require libsystemd-dev on Ubuntu 14.04 LTS

 -- Ondřej Surý <ondrej@debian.org>  Fri, 12 Aug 2016 10:45:09 +0200

knot-resolver (1.1.0~git2016072900-2) unstable; urgency=medium

  * Fix minor nits in maintainers scripts (Courtesy of Andreas Henriksson)
  * Imported Upstream version 1.1.0~2016080800
  * Bump required version of libknot to 2.3.0

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Aug 2016 10:04:01 +0200

knot-resolver (1.1.0~git2016072900-1) unstable; urgency=medium

  * Imported Upstream version 1.1.0~git2016072900
  * Knot Resolver is now running as non-privileged socket activated
    systemd service.

 -- Ondřej Surý <ondrej@debian.org>  Thu, 04 Aug 2016 09:01:15 +0200

knot-resolver (1.1.0~git2016072000-1) unstable; urgency=medium

  * Imported Upstream version 1.1.0~git2016072000

 -- Ondřej Surý <ondrej@debian.org>  Wed, 20 Jul 2016 14:31:10 +0200

knot-resolver (1.1.0~git2016071900-1) unstable; urgency=medium

  * Use correct lua comments in default kresd.conf
  * knot-resolver 1.1.0~git2016071300-3
  * Imported Upstream version 1.1.0~git2016071900

 -- Ondřej Surý <ondrej@debian.org>  Wed, 20 Jul 2016 07:45:42 +0200

knot-resolver (1.1.0~git2016071300-3) unstable; urgency=medium

  [ Daniel Kahn Gillmor ]
  * Remove static lib/libkres.a during clean
  * Put knot-resolver-doc into Section: doc (thanks, Lintian!)

  [ Ondřej Surý ]
  * Use correct lua comments in default kresd.conf

 -- Ondřej Surý <ondrej@debian.org>  Mon, 18 Jul 2016 10:47:12 +0200

knot-resolver (1.1.0~git2016071300-2) unstable; urgency=medium

  * Build and install documentation into knot-resolver-doc package
    (Closes: #831461)
  * Build with libsystemd-dev to get socket-activation
  * Set LD_LIBRARY_PATH to load correct libkresd.so.1 for tests
  * Link the tests with static libkres library

 -- Ondřej Surý <ondrej@debian.org>  Sat, 16 Jul 2016 15:55:04 +0200

knot-resolver (1.1.0~git2016071300-1) unstable; urgency=medium

  * Imported Upstream version 1.1.0~git2016071300
  * knot-resolver-module-all should be arch:all (only lua there)
  * Don't build tinyweb module anymore as HTTP/2 module replaces it

 -- Ondřej Surý <ondrej@debian.org>  Thu, 14 Jul 2016 07:28:24 +0200

knot-resolver (1.1.0~git2016070600-4) unstable; urgency=medium

  * Add missing http.lua to knot-resolver-module-http package

 -- Ondřej Surý <ondrej@debian.org>  Wed, 13 Jul 2016 15:35:11 +0200

knot-resolver (1.1.0~git2016070600-3) unstable; urgency=medium

  * Add NEW packages lua-http and lua-mmdb to knot-resolver-module-http
    Depends
  * knot-resolver now recommends knot-resolver-module-http
  * Add liblmdb-dev to Build-Depends (Closes: #830934)

 -- Ondřej Surý <ondrej@debian.org>  Wed, 13 Jul 2016 14:09:12 +0200

knot-resolver (1.1.0~git2016070600-2) unstable; urgency=medium

  * Explicitly declare architectures for tinyweb module
    (Closes: #830939)

 -- Ondřej Surý <ondrej@debian.org>  Wed, 13 Jul 2016 09:32:30 +0200

knot-resolver (1.1.0~git2016070600-1) unstable; urgency=medium

  * Imported Upstream version 1.1.0~git2016070600
  * Adapt to the new fixed config.mk that accepts overrides
  * Add new HTTP/2 module that will replace tinyweb module
  * Add daf module to knot-resolver
  * Bump standards to 3.9.8 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 11 Jul 2016 15:33:21 +0200

knot-resolver (1.0.0-16-g1070c49-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0-16-g1070c49
  * Add Breaks/Replaces for knot-resolver-module-tinyweb (Closes: #829051)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 30 Jun 2016 11:02:06 +0200

knot-resolver (1.0.0-13-g95b243f-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0-13-g95b243f

 -- Ondřej Surý <ondrej@debian.org>  Wed, 22 Jun 2016 09:28:44 +0200

knot-resolver (1.0.0-12-g86f75f0-1) unstable; urgency=medium

  * Split tinyweb module into separate package
  * Imported Upstream version 1.0.0-12-g86f75f0
  * Use simpler method for optional dh_strip parameter

 -- Ondřej Surý <ondrej@debian.org>  Tue, 21 Jun 2016 09:23:27 +0200

knot-resolver (1.0.0-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0

 -- Ondřej Surý <ondrej@debian.org>  Tue, 31 May 2016 10:12:26 +0200

knot-resolver (1.0.0~beta3-95-g550ceca-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-95-g550ceca

 -- Ondřej Surý <ondrej@debian.org>  Mon, 30 May 2016 09:38:02 +0200

knot-resolver (1.0.0~beta3-91-g9259b27-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-91-g9259b27

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 May 2016 14:51:19 +0200

knot-resolver (1.0.0~beta3-85-g503adee-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-85-g503adee

 -- Ondřej Surý <ondrej@debian.org>  Wed, 25 May 2016 09:19:05 +0200

knot-resolver (1.0.0~beta3-83-gc416877-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-83-gc416877

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 May 2016 14:24:41 +0200

knot-resolver (1.0.0~beta3-78-g3429619-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-78-g3429619

 -- Ondřej Surý <ondrej@debian.org>  Wed, 18 May 2016 10:33:19 +0200

knot-resolver (1.0.0~beta3-72-g4ffb749-1) unstable; urgency=medium

  * Move lintian overrides for sources to d/source.lintian-overrides
  * Add missing wildcard in lintian overrides
  * Require golang-go (>= 1.5.0) that's needed
  * Fix order of version requirement and arch for golang-go
  * Imported Upstream version 1.0.0~beta3-72-g4ffb749
  * Dynamic Go modules are supported on arm*, amd64 and i386 since
    golang-go (>= 1.6.0)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 12 May 2016 15:32:22 +0200

knot-resolver (1.0.0~beta3-71-gb5b0232-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-71-gb5b0232
  * Back to master branch

 -- Ondřej Surý <ondrej@debian.org>  Wed, 11 May 2016 13:36:37 +0200

knot-resolver (1.0.0~beta3-61-gc23edd0-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-61-gc23edd0
  * Pull fixes from tcp-ooo branch

 -- Ondřej Surý <ondrej@debian.org>  Tue, 03 May 2016 09:55:40 +0200

knot-resolver (1.0.0~beta3-60-ge61c48e-3) unstable; urgency=medium

  * Add a versioned Built-Using for golang-github-abh-geoip-dev
    (Courtesy of Michael Hudson-Doyle)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 26 Apr 2016 13:18:16 +0200

knot-resolver (1.0.0~beta3-60-ge61c48e-2) unstable; urgency=medium

  * Don't use dh_golang for computing Built-Using as it doesn't work,
    so we can build tinyweb module again

 -- Ondřej Surý <ondrej@debian.org>  Tue, 26 Apr 2016 11:40:30 +0200

knot-resolver (1.0.0~beta3-60-ge61c48e-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-60-ge61c48e
  * Remove -dbg package for dbgsym transition
  * Don't build tinyweb module as dh_golang is broken right now
    (Closes: #822386)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 26 Apr 2016 10:35:56 +0200

knot-resolver (1.0.0~beta3-41-g5bf6748-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-41-g5bf6748

 -- Ondřej Surý <ondrej@debian.org>  Wed, 13 Apr 2016 21:55:07 +0200

knot-resolver (1.0.0~beta3-31-g79a8440-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta3-31-g79a8440
  * Add kresd.8 man page to distribution
  * Fix WS in d/control

 -- Ondřej Surý <ondrej@debian.org>  Thu, 31 Mar 2016 18:46:30 +0200

knot-resolver (1.0.0~beta3-1) unstable; urgency=medium

  * Add lua-socket and lua-sec for root TA bootstrapping
  * Imported Upstream version 1.0.0~beta3

 -- Ondřej Surý <ondrej@debian.org>  Sat, 30 Jan 2016 16:15:23 +0100

knot-resolver (1.0.0~beta2-123-g6134920-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta2-123-g6134920
  * Mangle tinyweb only if it exists (Closes: #812276)
  * Mute lintian error about missing tinyweb.js sources and mute lintian
    warnings about shared library

 -- Ondřej Surý <ondrej@debian.org>  Tue, 26 Jan 2016 15:00:54 +0100

knot-resolver (1.0.0~beta2-104-ge110f97-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta2-104-ge110f97
  * Rebase patches on top of 1.0.0~beta2-104-ge110f97 release
  * Knot Resolver needs libknot 2.1.0 at least
  * Disable PIE hardening as it breaks the build
  * Squash debian quirks patch into one
  * Disable ASAN sanitized build
  * libkresd is now dynamic library
  * Install other new files into the package

 -- Ondřej Surý <ondrej@debian.org>  Tue, 12 Jan 2016 16:45:42 +0100

knot-resolver (1.0.0~beta2-58-gd8762fb-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta2-58-gd8762fb

 -- Ondřej Surý <ondrej@debian.org>  Fri, 04 Dec 2015 15:16:43 +0100

knot-resolver (1.0.0~beta2-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0~beta2
  * Build the Go modules on amd64
  * Compile the experimental with clang and AddressSanitizer
  * Fix missing GPL-2 and s/MIT/Expat/ in d/copyright
  * Add missing sources for javascript files
  * Add ${misc:Depends} to -dbg package
  * Tidy-up the permissions on modules
  * Use libjs-jquery and libjs-d3 instead of bundled javascript
  * Stop depending on libknot-dev as the code can cope with SOVERSION

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Nov 2015 11:49:25 +0100

knot-resolver (1.0.0~beta1-85-gd94caa6-1) experimental; urgency=medium

  * Imported Upstream version 1.0.0~beta1-85-gd94caa6

 -- Ondřej Surý <ondrej@debian.org>  Thu, 12 Nov 2015 23:14:07 +0100

knot-resolver (1.0.0~beta1-75-g0497be2-1) experimental; urgency=medium

  * Imported Upstream version 1.0.0~beta1-75-g0497be2
  * Rebase patches on top of 1.0.0~beta1-75-g0497be2

 -- Ondřej Surý <ondrej@debian.org>  Wed, 11 Nov 2015 09:58:20 +0100

knot-resolver (1.0.0~beta1-12-g822d8fe-1) experimental; urgency=medium

  * Initial release
  * Update d/copyright with some missing bits
  * Imported Upstream version 1.0.0~beta1-12-g822d8fe

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Oct 2015 11:55:14 +0200
